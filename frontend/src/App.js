import React, { useEffect, useState } from 'react';

import api from './services/api';
import Products from './components/products'

import './rules.css'
import './App.css';

import logo from './media/logo.png';
import searchIcon from './media/search.png';

function App() {
  const [categories, setCategories] = useState([]);
  const [currentPage, setCurrentPage] = useState('');
  const [ categorieId, setCategorieId ] = useState(0);

  useEffect(() => {
    async function chargeProductsName() {
      const response = await api.get('/categories/list')

      setCategories(response.data.items);

    }

    chargeProductsName();


  }, [])

  function setPageAndId(page, id) {
    
    setCurrentPage(page);
    setCategorieId(id);

  }

  console.log(categorieId)
  console.log(currentPage)


  return (
    <>
      <footer className="login-bar"><a href="#">Acesse sua Conta</a> ou <a href="#">Cadastre-se</a></footer>

      <header className="header">
        <input id="menu-click" type="checkbox" />
        <label >
          <div className="mobile-menu">
            <span></span>
          </div>
        </label>
        <img src={logo} />
        <div className="search">
          <input className="search-bar" type="text" />
          <button className="btn search-btn">BUSCAR</button>
          <img src={searchIcon} width="40" height="40" className="search-button" />
        </div>
      </header>

      <nav className="menu">
        <a href="/">Página Inicial</a>
        {categories.map(categorie => (
          <a
            onClick={event => setPageAndId(event.target.text, categorie.id)}
          >{categorie.name}
          </a>
        ))}
      </nav>


      <div className="container">

        <div className="current-page">
          <span>Página Inicial > <p className="current-page">{currentPage}</p></span>
        </div>

        <div className="products">

          <Products categorieId={categorieId}/>
        </div>

      </div>
    </>
  );
}

export default App;
