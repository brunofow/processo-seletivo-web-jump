import React, { useEffect, useState } from 'react';

import api from '../../services/api';

import './styles.css'


function Products({ categorieId }) {
  const [products, setProducts] = useState([]);



  useEffect(() => {
    async function loadProducts() {
      const response = await api.get(`/categories/${categorieId}`)

      setProducts(response.data);
    }

    loadProducts();
  }, [categorieId]);

  console.log(products);


  return (
    <>
      <div className="products">
        <div className="product">
          <img className="product-image" src={require('../../media/pants-1.jpg')} />
          <span className="product-name">Calças</span>
          <span className="price">R$40,00</span>

          <button className="btn buy-btn">COMPRAR</button>
        </div>
      </div>
    </>
  );
}

export default Products